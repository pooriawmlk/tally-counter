package sbu.cs;

import java.util.ArrayList;
import java.util.List;

public class ExerciseLecture6
{
    public long calculateEvenSum(int[] arr)
    {
        long sum = 0;

        for(int i = 0; i < arr.length; i += 2)
        {
            sum += arr[i];
        }

        return sum;

    }

    public int[] reverseArray(int[] arr)
    {
        int[] reversedArr = new int[arr.length];

        for(int i = 0, j = arr.length - 1; i < arr.length; i++, j--)
        {
            reversedArr[j] = arr[i];
        }

        return reversedArr;
    }

    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException
    {
        int row1 = m1.length;
        int col1 = m1[0].length;
        int row2 = m2.length;
        int col2 = m2[0].length;

        if(col1 != row2)
        {
            throw new RuntimeException();
        }

        double[][] product = new double[row1][col2];

        for(int i = 0; i < row1; i++)
        {
            for(int j = 0; j < col2; j++)
            {
                for(int k = 0; k < row2; k++)
                {
                    product[i][j] += m1[i][k] * m2[k][j];
                }
            }
        }

        return product;
    }

    public List<List<String>> arrayToList(String[][] names)
    {
        List<List<String>> namesList = new ArrayList<List<String>>();

        for(int i = 0; i < names.length; i++)
        {
            ArrayList<String> segment = new ArrayList<>();

            for(int j = 0; j < names[0].length; j++)
            {
                segment.add(names[i][j]);
            }

            namesList.add(segment);
        }

        return namesList;
    }

    public List<Integer> primeFactors(int n)
    {
        List<Integer> primeNums = new ArrayList<>();

        double limit = n;

        boolean[] isAdded = new boolean[n+1];

        for(int i = 2; i <= limit; i++)
        {
            while(n % i == 0)
            {
                n /= i;

                if(!isAdded[i])
                {
                    primeNums.add(i);

                    isAdded[i] = true;
                }

            }
        }

        return primeNums;
    }

    public List<String> extractWord(String line)
    {
        line = line.replaceAll("[)(*&^%$#@!,?><+1234567890]", "");

        String[] wordsArr = line.split(" ");

        List<String> words = new ArrayList<>();

        for(int i = 0; i < wordsArr.length; i++)
        {
            words.add(wordsArr[i]);
        }

        return words;
    }
}
