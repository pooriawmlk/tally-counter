package sbu.cs;

import java.util.Random;

public class ExerciseLecture5
{

    public String weakPassword(int length)
    {
        Random randomChar = new Random();

        StringBuilder password = new StringBuilder();

        for(int i = 0; i < length; i++)
        {
            password.append((char)(randomChar.nextInt(26) + 'a'));
        }
        String finalPassword = password.toString();

        return finalPassword;
    }

    public String strongPassword(int length) throws Exception
    {
        if(length < 3)
        {
            throw new IllegalValueException();
        }

        Random random = new Random();

        String specialChars = "!@#$%^&*()_+~";
        int randomNum = random.nextInt(10);
        int randomIndex = random.nextInt(10);

        StringBuilder password = new StringBuilder();

        for(int i = 0; i < (length-2)/2; i++)
        {
            password.append((char)(random.nextInt(26) + 'a'));
        }

        password.append(randomNum);

        for(int i = (length)/2; i < length-1; i++)
        {
            password.append((char)(random.nextInt(26) + 'a'));
        }

        password.append(specialChars.charAt(randomIndex));


        return password.toString();
    }

    public boolean isFiboBin(int n)
    {
        int lastFib = 0;
        int currFib = 1;

        boolean isFibobin = false;

        for(int i = 1; i < n; i++)
        {
            int fiboBin = 0;

            int tmp = currFib;
            currFib += lastFib;
            lastFib = tmp;

            tmp = currFib;
            int count1 = 0;

            while(tmp > 0)
            {
                count1 += tmp & 1;

                tmp >>= 1;
            }

            fiboBin = count1 + currFib;

            if(fiboBin == n)
            {
                isFibobin = true;
                break;
            }

        }

        return isFibobin;
    }
}
