package sbu.cs;

import java.util.Locale;

public class ExerciseLecture4
{
    public long factorial(int n)
    {
        long ans = 1;

        for (int i = 1; i <= n; i++)
        {
            ans *= i;
        }

        return ans;
    }

    public long fibonacci(int n)
    {
        long lastFib = 0;
        long currFib = 1;

        for(int i = 1; i < n; i++)
        {
            long tmp = currFib;
            currFib += lastFib;
            lastFib = tmp;
        }

        return currFib;
    }

    public String reverse(String word)
    {

        return new StringBuilder(word).reverse().toString();
    }

    public boolean isPalindrome(String line)
    {
        boolean isP = true;

        line = line.toLowerCase(Locale.ROOT);

        StringBuilder lineWithNoSpace = new StringBuilder(line);

        for(int i = 0; i < lineWithNoSpace.length(); i++)
        {
            if(lineWithNoSpace.charAt(i) == ' ')
            {
                lineWithNoSpace.deleteCharAt(i);
                i--;
            }
        }

        int size = lineWithNoSpace.length();

        for(int i = 0, j = size - 1; i < size/2; i++, j--)
        {
            if(lineWithNoSpace.charAt(i) != lineWithNoSpace.charAt(j))
            {
                isP = false;
                break;
            }
        }

        return isP;
    }

    public char[][] dotPlot(String str1, String str2)
    {
        int row = str1.length();
        int col = str2.length();

        char[][] dots = new char[row][col];

        for(int i = 0; i < row; i++)
        {
            for(int j = 0; j < col; j++)
            {
                if(str1.charAt(i) == str2.charAt(j))
                {
                    dots[i][j] = '*';
                }
                else
                {
                    dots[i][j] = ' ';
                }
            }
        }

        return dots;
    }
}
